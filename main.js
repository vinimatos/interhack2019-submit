var evasionController = (function () {

    var Evaded = function(name, income, average, frequency, distance, id) {
        this.name = name;
        this.income = income;
        this.average = average;
        this.frequency = frequency;
        this.distance = distance;
        this.id = id;
    }

    var Enrolled = function(name, income, average, frequency, distance, id) {
        this.name = name;
        this.income = income;
        this.average = average;
        this.frequency = frequency;
        this.distance = distance;
        this.id = id;
    }

    var data = {
        allItems: {
            enr: [],
            evd: []
        }
    }

    var nameAndIncome = function() {
        var nameIncome = []
        for (var i=0; i < data.allItems.evd.length; i++){
            nameIncome[i]= [data.allItems.evd[i].name,data.allItems.evd[i].income]
        }
    }

    var ArranjoAlunos = [];
    
    var Alunos = function(name, risco) {
        this.name = name;
        this.risco = risco;
        this.dps = [];
    }
    
    var calculaDP = function(num) {

        var soma =0;   
        for (var i=0; i < data.allItems.evd.length; i++){
            
            if(num==0) tipo = data.allItems.evd[i].average
            else if(num==1) tipo = data.allItems.evd[i].distance
            else if(num==2) tipo = data.allItems.evd[i].frequency
           // else if(num==3) tipo = data.allItems.evd[i].income
            soma += tipo;
        }
        media = soma/data.allItems.evd.length;
        // console.log("media"+media);
        var arr=[];
        for(var j = 0; j < data.allItems.enr.length; j++){
            if(num==0) tipo = data.allItems.enr[j].average
            else if(num==1) tipo = data.allItems.enr[j].distance
            else if(num==2) tipo = data.allItems.enr[j].frequency
            //else if(num==3) tipo = data.allItems.enr[j].income

            varianca = Math.abs(tipo - media)
            dp = Math.sqrt(varianca);
            // console.log(dp);
            arr[j] = dp;  
            

        }

        return arr;
    }                                                                     

    var abasteceAlunos = function(){
        var dp1 = calculaDP(0);
        var dp2 = calculaDP(1);
        var dp3 = calculaDP(2);
        //var dp4 = calculaDP(3);
        var aux = 0;

        ArranjoAlunos = [];
        
        for (var x = 0; x < data.allItems.enr.length; x++){
            var aluno = new Alunos(data.allItems.enr[x].name,""); 
            
            aluno.id = data.allItems.enr[x].id;
            aluno.dps[0]=dp1[x]
            aluno.dps[1]=dp2[x]
            aluno.dps[2]=dp3[x]
           //aluno.dps[3]=dp4[x]
            ArranjoAlunos.push(aluno);

        }
        calculaRisco()

        nameAndIncome()
    }
    var calculaRisco = function() {
        var soma =0;   
        for (var i=0; i < ArranjoAlunos.length; i++){

            if(ArranjoAlunos[i].dps[0]<= 2) soma += 1
            if(ArranjoAlunos[i].dps[1]<=5) soma += 1
            if(ArranjoAlunos[i].dps[2]<=5) soma += 1
            
            if(soma == 3) ArranjoAlunos[i].risco = (75 * (1 + (ArranjoAlunos[i].dps[0] + ArranjoAlunos[i].dps[1]+ ArranjoAlunos[i].dps[2])/150)).toFixed(2);
            else if(soma == 2) ArranjoAlunos[i].risco = (56.25 * (1 + (ArranjoAlunos[i].dps[0] + ArranjoAlunos[i].dps[1]+ ArranjoAlunos[i].dps[2])/150)).toFixed(2);
            else if(soma == 1) ArranjoAlunos[i].risco = (37.5 * (1 + (ArranjoAlunos[i].dps[0] + ArranjoAlunos[i].dps[1]+ ArranjoAlunos[i].dps[2])/150)).toFixed(2);
            else if(soma == 0) ArranjoAlunos[i].risco = (18.75 * (1 + (ArranjoAlunos[i].dps[0] + ArranjoAlunos[i].dps[1]+ ArranjoAlunos[i].dps[2])/150)).toFixed(2);

            soma = 0;
        }
    }

    return {
        addItem: function (name, income, average, frequency, distance, type) {
            var newItem, ID;
                
                //Create a new ID
                if(data.allItems[type].length > 0) 
                ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
                else ID = 0;
                
                if(type == 'enr') {
                //Create a new Item 
                newItem = new Enrolled (name, income, average, frequency, distance, ID);
                }

                else newItem = new Evaded (name,income, average, frequency, distance, ID);

                //Push it into our data structure
                data.allItems[type].push(newItem);  
                
                abasteceAlunos();
                // distanciaMedia()
                //Return the new element
                return newItem;
        },

        deleteItem: function (ID, type) {
            var ids, index;
                
            ids = data.allItems[type].map(function(current) {
                return current.id;
            });
            
            index = ids.indexOf(ID);
            // nameIncome = [data.allItems[type][index].name,data.allItems[type][index].income]

            if(index !== -1) {
                data.allItems[type].splice(index, 1);
            }
            
            abasteceAlunos();
         

        },

        evadeEnrolled: function(id) {
            var ids, index;

            ids = data.allItems.enr.map(function(current) {
                return current.id;
            });
            
            index = ids.indexOf(id);

            // console.log(index);

            return data.allItems.enr[index];
        },
        
        calculaDistanciaMedia: function() {
            var soma =0;   
            for (var i=0; i < data.allItems.evd.length; i++){
               soma += data.allItems.evd[i].distance
            }
            media = soma/data.allItems.evd.length;
            //console.log('Media: '+ media);
            return media;
            
        },
        
        getAlunos: function () {
            return ArranjoAlunos;
        },

        ctrlAbasteceAlunos: function () {
            abasteceAlunos();
        },

        ordenaAlunosPerc: function (alunos) {
            var alunosOrdenados = alunos.sort((a,b) => {
                if(a.risco > b.risco) return -1;

                if(a.risco < b.risco) return 1;

                return 0;
            }) 
            
            console.log(alunosOrdenados);
            return alunosOrdenados;
        },

        calculaPorcentagemRenda: function () {
            var porcentagens = [];

            console.log(data.allItems.evd);
            for (var x = 0; x < data.allItems.evd.length; x++){
                porcentagens.push(0); 
             }

            for (var i=0; i < data.allItems.evd.length; i++){
                porcentagens[parseInt(data.allItems.evd[i].income)]++;
             }

             for (var j=0; j < data.allItems.evd.length; j++){
                porcentagens[j] = (porcentagens[j] / data.allItems.evd.length);
             }

             console.log(porcentagens);

             return porcentagens;
        },

        getDados: function() {
            return data;
        }
    }

})();

var UIController = (function () {
    var DOMstrings = {
        addButton: '.add__btn',
        inputName: '.add__name',
        inputIncome: '.add__income',
        inputAverage: '.add__average',
        inputFrequency: '.add__frequency',
        inputDistance: '.add__distance',
        enrolledContainer: '.enrolled__list',
        evadedContainer: '.evaded__list',
        deleteButton: '.item__delete-btn',
        container: '.container',
        distanceValue: '.distance__value',
        distanceTime: '.distance__time--value',
        riskValue: '.item__value',
        riskItemName: '.risk__item--name',
        riskItemPercentage: '.risk__item--percentage'
    }

    var nodeListForEach = function(list,callback) {
        for(var i = 0; i<list.length; i++) {
            callback(list[i], i); 
        }
    };

    return {
        getDOMstrings: function () {
            return DOMstrings;
        },

        getInput: function () {
            return {
                name: document.querySelector(DOMstrings.inputName).value,
                income: document.querySelector(DOMstrings.inputIncome).value,
                average: parseFloat(document.querySelector(DOMstrings.inputAverage).value),
                frequency: parseFloat(document.querySelector(DOMstrings.inputFrequency).value),
                distance: parseFloat(document.querySelector(DOMstrings.inputDistance).value)
            };
        },

        addListItem: function (newItem, type) {
            var html,newHtml, element;
         
            //Create HTML string with placeholder text
            
            if(type == 'enr') {
                element = DOMstrings.enrolledContainer;
                html = '<div class="item" id="%type%-%id%"><div class="item__description">%name%</div><div class="item__value">90%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div>';
            } 
            else {
                element = DOMstrings.evadedContainer;
                html = '<div class="item" id="%type%-%id%"><div class="item__description">%name%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div>';
            }
           
            //Replace the placeholder text with some actual data

            newHtml = html.replace('%id%', newItem.id);
            newHtml = newHtml.replace('%name%', newItem.name);
            newHtml = newHtml.replace('%type%', type);

            //Insert the HTML into the dom
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },

        deleteListItem: function (selectorID) {
            var element =  document.getElementById(selectorID);
            element.parentNode.removeChild(element);
        },

        clearFields: function () {
            var fields, fieldsArray;
            
            fields = document.querySelectorAll(DOMstrings.inputName + ',' + DOMstrings.inputAverage + ',' + DOMstrings.inputFrequency + ',' + DOMstrings.inputDistance);   

            fieldsArray = Array.prototype.slice.call(fields);

            fieldsArray.forEach(function (current, index, array) {
                current.value = "";
            });

            fieldsArray[0].focus();
        },
        
        displayDistance: function (distance) {
            if(isNaN(distance)) distance = 0;
            document.querySelector(DOMstrings.distanceValue).textContent = `${distance.toFixed(2)} Km`;
            document.querySelector(DOMstrings.distanceTime).textContent = `${(distance/23).toFixed(1)}`;
        },

        displayRisks: function (arrAlunos) {
            var fields;
            fields = document.querySelectorAll(DOMstrings.riskValue);

            nodeListForEach(fields, function(current,index) {
                current.textContent = arrAlunos[index].risco + ' %';
            });
        }, 

        displayTop: function (alunosOrdenados) {
            var fields1, fields2;
            fields1 = document.querySelectorAll(DOMstrings.riskItemName);
            fields2 = document.querySelectorAll(DOMstrings.riskItemPercentage);

            nodeListForEach(fields1, function(current,index) {
                current.textContent = `${index + 1}. ${alunosOrdenados[index].name}`;
            });

            nodeListForEach(fields2, function(current,index) {
                current.textContent = `${alunosOrdenados[index].risco} %`;
            });
        }
    }
})();

var controller = (function (evdCtrl, UICtrl) {

    // Radialize the colors
Highcharts.setOptions({
    colors: ['#ba68c8', '#ab47bc', '#9c27b0', '#8e24aa']
});

    var ctrlAddItem = function () {
        var input, newItem;
        
        // 1. Get the field input data
        input = UICtrl.getInput();
    
        if(input.name !== '' && input.income !== '-1' && input.frequency >= 0 && input.frequency <= 100 && input.average >= 0 && input.average <= 10) {
        
            // 2. Add the item to the evasion controller
            newItem = evdCtrl.addItem(input.name, input.income, input.average, input.frequency, input.distance, 'enr');
            // console.log(newItem);

            // 3. Add the new item to the UI
            UICtrl.addListItem(newItem, 'enr');

            // 4. Clear the fields
            UICtrl.clearFields();

            //5. Update the distance
            updateDistance();

            //6. Update Risk Percentage
            var alunos = evdCtrl.getAlunos();
            UICtrl.displayRisks(alunos);
            var alunosOrdenados = evdCtrl.ordenaAlunosPerc(alunos);
           UICtrl.displayTop(alunosOrdenados); 

           //7. Atualiza o gráfico
           atualizaGrafico();
        }
    }

    var ctrlDeleteItem = function (event) {
        var itemID, splitID, ID;
        itemID = event.target.parentNode.parentNode.parentNode.id;
        
        if(itemID) {

            splitID = itemID.split('-');
            type = splitID[0];
            ID = parseInt(splitID[1]);

            if(type == 'enr') {
                var item = evdCtrl.evadeEnrolled(ID);
                // console.log(item);
                var newItem = evdCtrl.addItem(item.name, item.income, item.average, item.frequency, item.distance, 'evd');
                UICtrl.addListItem(newItem, 'evd');
            }

            // 1. Delete the item from the data structure
            x = evdCtrl.deleteItem(ID,type);
            console.log(x)
            // 2. Delete the item from the UI
            UICtrl.deleteListItem(itemID);

            //3. Update the distance
            updateDistance();

            //4. Atualiza o risco
            var alunos = evdCtrl.getAlunos();
            UICtrl.displayRisks(alunos);
            var alunosOrdenados = evdCtrl.ordenaAlunosPerc(alunos);
            UICtrl.displayTop(alunosOrdenados); 

            //5. Atualiza o gráfico
            atualizaGrafico();

        }
    }

    var updateDistance = function () {
        var distancia = evdCtrl.calculaDistanciaMedia();
        UICtrl.displayDistance(distancia);
    }

    var ctrlUpdateRisk = function () {
        evdCtrl.updateRisk();
    }

    var alimentaBase = function (nome, renda, media, frequencia, distancia, tipo) {
        var newItem = evdCtrl.addItem(nome, renda, media , frequencia, distancia, tipo);
        UICtrl.addListItem(newItem, tipo);
        updateDistance();
    }

    var atualizaGrafico = function () {
        var dados = evdCtrl.getDados();
        var porcentagens = evdCtrl.calculaPorcentagemRenda();

        var paramChart = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Taxa de Evasão Média por Renda (em Salários Mínimos)'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        connectorColor: 'silver'
                    }
                }
            },
            series: [{
                name: 'Porcentagem',
                data: [
                    { name: '< 1', y: porcentagens[0]},
                    { name: '1~3', y: porcentagens[1] },
                    { name: '4~6', y: porcentagens[2] },
                    { name: '> 6', y: porcentagens[3] },
                ]
            }]
        }

    

    // Build the chart
    Highcharts.chart('container', paramChart);
    }

    
    
    var setUpEventListeners = function () {
        
        var DOMstrings = UICtrl.getDOMstrings();
        
        document.querySelector(DOMstrings.addButton).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function(event) {
            
            if(event.keyCode === 13 || event.which === 13) {
                ctrlAddItem();
            }
        });

        document.querySelector(DOMstrings.container).addEventListener('click', ctrlDeleteItem);

        //document.querySelector(DOMstrings.inputType).addEventListener('change', UICtrl.changedType);
    }

    return {
        init: function () {
            setUpEventListeners();
            updateDistance();
            alimentaBase('Victor Pereira', '1', 4, 52, 40, 'evd');
            alimentaBase('Carlos Henrique', '3', 3.15, 54, 37, 'evd');
            alimentaBase('Raphaela dos Santos', '3', 3.5, 51, 38, 'evd');
            alimentaBase('Mariela da Souza', '3', 3.3, 53, 37, 'evd');
            alimentaBase('Gustavo Oliveira', '3', 3.4, 54, 49, 'evd');
            alimentaBase('Renan Machado', '3', 3.5, 53, 40, 'evd');
            alimentaBase('Paulo Ferreira', '2', 5, 40, 50, 'evd');
            alimentaBase('Maria Julia', '0', 3, 20, 80, 'evd');
            alimentaBase('Lucas Vieira', '2', 5, 70, 50, 'evd');
            alimentaBase('Joana Silva', '0', 3, 72, 80, 'evd');
            alimentaBase('Claudia Pinto', '2', 5.2, 90, 7, 'enr');
            alimentaBase('Carina Huang', '1', 3, 50, 42, 'enr');
            alimentaBase('Felipe Matos', '2', 9, 75, 11, 'enr');
            alimentaBase('Nicole Souza', '2', 9.5, 80, 63, 'enr');
            alimentaBase('Alessandro Costa', '3', 8.5, 45, 24, 'enr');
            alimentaBase('Flavio Oliveira', '3', 7.2, 63, 18, 'enr');
            alimentaBase('Vinicius Matos', '2', 2.6, 53, 7, 'enr');
            alimentaBase('Nicole Oliveira', '3', 7.2, 75, 18, 'enr');
            alimentaBase('Katarina Wahu', '1', 3, 50, 32, 'enr');
            alimentaBase('Fernando Araujo', '2', 9.5, 55, 11, 'enr');
            alimentaBase('Melissa Matos', '2', 9.35, 13, 7, 'enr');
            alimentaBase('Rodrigo Lima', '3', 5.35, 10, 24, 'enr');
            var alunos = evdCtrl.getAlunos();
            UICtrl.displayRisks(alunos);
            var alunosOrdenados = evdCtrl.ordenaAlunosPerc(alunos);
            UICtrl.displayTop(alunosOrdenados); 
            atualizaGrafico();
        }
    }

})(evasionController, UIController);

controller.init();


